package com.example.mg.inventorydatabase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.mg.inventorydatabase.BookInventory;

import java.util.ArrayList;
import java.util.List;


public class InventoryOperations {
    public static final String LOGTAG = "INV_BOOK_SYS";

    SQLiteOpenHelper dbhandler;
    SQLiteDatabase database;

    private static final String[] allColumns = {
            InventoryDBHandler.COLUMN_BOOK_ID,
            InventoryDBHandler.COLUMN_BOOK_NAME,
            InventoryDBHandler.COLUMN_BOOK_AUTHOR,
            InventoryDBHandler.COLUMN_ISBN

    };

    public InventoryOperations(Context context){
        dbhandler = new InventoryDBHandler(context);
    }

    public void open(){
        Log.i(LOGTAG,"Database Opened");
        database = dbhandler.getWritableDatabase();


    }
    public void close(){
        Log.i(LOGTAG, "Database Closed");
        dbhandler.close();

    }
    public BookInventory addBook(BookInventory Book){
        ContentValues values  = new ContentValues();
        values.put(InventoryDBHandler.COLUMN_BOOK_ID, Book.getBookId());
        values.put(InventoryDBHandler.COLUMN_BOOK_NAME, Book.getBookName());
        values.put(InventoryDBHandler.COLUMN_BOOK_AUTHOR,Book.getBookAuthor());
        values.put(InventoryDBHandler.COLUMN_ISBN, Book.getISBN());
        //Not sure about this part???
        long insertid = database.insert(InventoryDBHandler.TABLE_BOOKS,null,values);
        Book.setBookId(insertid);
        return Book;

    }

    // Getting single Book
    public BookInventory getBook(long id) {

        Cursor cursor = database.query(InventoryDBHandler.TABLE_BOOKS,allColumns,InventoryDBHandler.COLUMN_BOOK_ID + "=?",new String[]{String.valueOf(id)},null,null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        BookInventory e = new BookInventory(Long.parseLong(cursor.getString(0)),Long.parseLong(cursor.getString(1)),cursor.getString(2),cursor.getString(3));
        // return Employee
        return e;
    }

    public List<BookInventory> getAllBooks() {

        Cursor cursor = database.query(InventoryDBHandler.TABLE_BOOKS,allColumns,null,null,null, null, null);

        List<BookInventory> books = new ArrayList<>();
        if(cursor.getCount() > 0){
            while(cursor.moveToNext()){
                BookInventory book = new BookInventory();
                book.setBookId(cursor.getLong(cursor.getColumnIndex(InventoryDBHandler.COLUMN_BOOK_ID)));
                book.setISBN(cursor.getLong(cursor.getColumnIndex(InventoryDBHandler.COLUMN_ISBN)));
                book.setBookAuthor(cursor.getString(cursor.getColumnIndex(InventoryDBHandler.COLUMN_BOOK_AUTHOR)));
                book.setBookName(cursor.getString(cursor.getColumnIndex(InventoryDBHandler.COLUMN_BOOK_NAME)));
                books.add(book);
            }
        }
        // return All books
        return books;
    }



    //Updating Book
    public int updateBook(BookInventory book) {

        ContentValues values = new ContentValues();
        values.put(InventoryDBHandler.COLUMN_BOOK_ID, book.getBookId());
        values.put(InventoryDBHandler.COLUMN_ISBN, book.getISBN());
        values.put(InventoryDBHandler.COLUMN_BOOK_AUTHOR, book.getBookAuthor());
        values.put(InventoryDBHandler.COLUMN_BOOK_NAME, book.getBookName());

        // updating row
        return database.update(InventoryDBHandler.TABLE_BOOKS, values,
                InventoryDBHandler.COLUMN_BOOK_ID + "=?",new String[] { String.valueOf(book.getBookId())});
    }

    // Deleting Book
    public void removeBook(BookInventory book) {

        database.delete(InventoryDBHandler.TABLE_BOOKS, InventoryDBHandler.COLUMN_BOOK_ID + "=" + book.getBookId(), null);
    }



}
