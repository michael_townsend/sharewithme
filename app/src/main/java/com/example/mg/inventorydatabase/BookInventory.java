package com.example.mg.inventorydatabase;

public class BookInventory {
    private String BookName;
    private String BookAuthor;
    private long ISBN;
    private long bookId;

    public BookInventory(long bookId, long ISBN, String bookName, String bookAuthor) {
        this.BookAuthor = bookAuthor;
        this.BookName = bookName;
        this.ISBN = ISBN;
        this.bookId = bookId;
    }

    public BookInventory() {

    }

    public String getBookName(){
        return BookName;
    }

    public String getBookAuthor(){
        return BookAuthor;
    }

    public long getISBN(){
        return ISBN;
    }

    public long getBookId(){
        return bookId;
    }

    public void setBookName(String bookName){
        this.BookName = bookName;
    }

    public void setBookAuthor(String bookAuthor){
        this.BookAuthor = bookAuthor;
    }

    public void setISBN(long ISBN){
        this.ISBN = ISBN;
    }

    public void setBookId(long bookId){
        this.bookId = bookId;
    }

    public String toString() {
        return "Book ID: " + getBookId() + "\n" +
                "Book Name: " + getBookName() + "\n" +
                "Book Author: " + getBookAuthor() + "\n" +
                "ISBN: " + getISBN();


    }

}
